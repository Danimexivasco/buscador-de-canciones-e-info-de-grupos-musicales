import React, { useState } from 'react';

const Formulario = ({guardarBusquedaLetra}) => {

    const [busqueda, guardarBusqueda] = useState({
        artista: '',
        titulo: ''
    })

    const [error, guardarError] = useState(false)

    // Extraer valores 
    const { artista, titulo } = busqueda;

    // Funcion para actualizar el state
    const actualizarState = e => {
        guardarBusqueda({
            ...busqueda,
            [e.target.name]: e.target.value
        })
    }

    const buscarInformacion = e => {
        e.preventDefault()

        // Validación
        if (artista.trim() === '' || titulo.trim() === '') {
            guardarError(true)
            return;
        }
        guardarError(false);

        // Pasamos la busqueda al componenente principal
        guardarBusquedaLetra(busqueda)
        console.log(busqueda)
    }
    return (

        <div className="bg-info">
            <div className="container">
                <div className="row">
                    <form
                        className="col card text-white bg-transparent mb-5 pt-5 pb-2"
                        onSubmit={buscarInformacion}
                    >

                        <fieldset>
                            <legend className="text-center">Buscador Letras Canciones</legend>
                            {error ? <p className="alert alert-danger text-center p-2">Todos los campos son obligatorios</p> : null}
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label>Artista</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="artista"
                                            placeholder="Nombre del artista"
                                            onChange={actualizarState}
                                            value={artista}
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label>Canción</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="titulo"
                                            placeholder="Título de la canción"
                                            onChange={actualizarState}
                                            value={titulo}
                                        />
                                    </div>
                                </div>
                            </div>
                            <button
                                type="submit"
                                className="btn btn-primary float-right"
                            >Buscar</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Formulario;