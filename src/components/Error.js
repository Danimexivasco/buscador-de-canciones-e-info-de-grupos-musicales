import React from 'react';
import Imagen from '../llorando.jpg'

const Error = ({msg}) => {
    return ( 
        <div className="text-center">
            <p className="alert alert-danger text-center p-2" >{msg}</p>
            <img src={Imagen} alt="Imagen Error"></img>
        </div>

     );
}
 
export default Error;