function PrimeraMayuscula (palabra) {
    palabra = palabra.split(" ");

    for (let i = 0, x = palabra.length; i < x; i++) {
        palabra[i] = palabra[i][0].toUpperCase() + palabra[i].substr(1);
    }

    return palabra.join(" ");
}
export default  PrimeraMayuscula;