import React, { Fragment, useState, useEffect } from 'react';
import axios from 'axios';
import Formulario from './components/Formulario';
import Letra from './components/Letra';
import Informacion from './components/Informacion';
import Error from './components/Error'

function App() {

  const [busquedaLetra, guardarBusquedaLetra] = useState({})
  const [letra, guardarLetra] = useState('')
  const [informacion, guardarInformacion] = useState({})
  const [errores, guardarErrores] = useState(false)
  



  useEffect(() => {
    if (Object.keys(busquedaLetra).length === 0) return;

    const llamadaAPILetra = async () => {
      const { artista, titulo } = busquedaLetra;
      const url = `https://api.lyrics.ovh/v1/${artista.trim()}/${titulo.trim()}`
      const url2 = `https://www.theaudiodb.com/api/v1/json/1/search.php?s=${artista.trim()}`
      try{

      
      const [letra, informacion] = await Promise.all([
        axios(url),
        axios(url2)
      ])
      guardarErrores(false)
      guardarLetra(letra.data.lyrics)
      guardarInformacion(informacion.data.artists[0])
    }catch(e){
      console.log(`Axios request failed: ${e}`);
      guardarLetra('')
      guardarInformacion('')
      guardarErrores(true)
    }
      
      // const resultado = await axios(url);
      // guardarLetra(resultado.data.lyrics)
    }
    llamadaAPILetra()
  }, [busquedaLetra])

  return (
    <Fragment>
      <Formulario
        guardarBusquedaLetra={guardarBusquedaLetra}
      />
      {errores? <Error msg='Oops no hemos encontrado nada con esa búsqueda... Prueba otra vez!!'/>: null}
      <div className="container mt-5">
        <div className="row">
          <div className="col-md-6">
            <Letra 
              letra = {letra}
              busquedaLetra = {busquedaLetra}
            />
          </div>
          <div className="col-md-6">
            <Informacion 
              informacion = {informacion}
              busquedaLetra = {busquedaLetra}
            />
          </div>

        </div>

      </div>

    </Fragment>
  );
}

export default App;
