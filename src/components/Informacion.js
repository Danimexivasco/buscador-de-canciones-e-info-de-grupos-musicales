import React, { Fragment } from 'react';
import PrimeraMayuscula from '../helpers';

const Informacion = ({ informacion, busquedaLetra }) => {
    if (Object.keys(informacion).length === 0) return null;

    const { strArtistThumb, intFormedYear, strGenre, intMembers, strCountryCode, strBiographyES, strBiographyEN, strFacebook, strTwitter, strLastFMChart } = informacion;
    return (
        <div className="card border-light">
            <div className="card-header bg-primary text-light font-weight-bold">
                Información sobre <b>{PrimeraMayuscula(busquedaLetra.artista.trim())}</b>
            </div>
            <div className="card-body">
                <img src={strArtistThumb} alt="Logo Grupo" />
                <div className="card-text mt-3">
                    <div className="row">
                        <div className="col md-6 ">
                            <p>Género: {strGenre}</p>
                            <p>Año de creación: {intFormedYear}</p>
                        </div>
                        <div className="col md-6 ">
                            <p>País: {strCountryCode}</p>
                            <p>Integrantes: {intMembers}</p>
                        </div>
                    </div>
                </div>
                <div className="card-text mt-3">

                    {strBiographyES ?
                        <Fragment>
                            <h2>Biografía</h2>
                            <p className="bio">{strBiographyES}</p>
                        </Fragment>
                        :
                        <Fragment>
                            <h2>Biografía<small className="soloIngles">(Solo disponible en Inglés)</small></h2>
                            <p className="bio">{strBiographyEN}</p>
                        </Fragment>
                    }

                    <p>
                        {strFacebook ?
                            <a href={`https://${strFacebook}`} target="_blank" rel="noopener noreferrer">
                                <i className="fab fa-facebook"></i>
                            </a>
                            :
                            null

                        }

                        {strTwitter ?
                            <a href={`https://${strTwitter}`} target="_blank" rel="noopener noreferrer">
                                <i className="fab fa-twitter"></i>
                            </a>
                            :
                            null

                        }


                        {strLastFMChart ?
                            <a href={`${strLastFMChart}`} target="_blank" rel="noopener noreferrer">
                                <i className="fab fa-lastfm"></i>
                            </a>
                            :
                            null

                        }

                    </p>
                </div>
            </div>

        </div>

    );
}

export default Informacion;