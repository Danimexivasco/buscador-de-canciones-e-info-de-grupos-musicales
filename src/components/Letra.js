import React, {Fragment} from 'react';
import PrimeraMayuscula from '../helpers';

const Letra = ({letra, busquedaLetra}) => {

    if(letra.length === 0) return null;
    return ( 
        <Fragment>
            <h2>Letra de "<b>{PrimeraMayuscula(busquedaLetra.titulo.trim())}</b>"</h2>
            <p className="letra">{letra}</p>
        </Fragment>
     );
}
 
export default Letra;